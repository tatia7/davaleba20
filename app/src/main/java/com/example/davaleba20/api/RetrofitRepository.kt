package com.example.davaleba20.api

import com.example.davaleba20.model.NewsModel
import retrofit2.Response
import retrofit2.http.GET

interface RetrofitRepository {

    @GET("v3/articles")
    suspend fun getNews(): Response<List<NewsModel>>

}
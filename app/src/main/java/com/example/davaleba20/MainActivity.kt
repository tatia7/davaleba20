package com.example.davaleba20

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.viewpager2.widget.ViewPager2
import com.example.davaleba20.adapters.recViewAdapter
import com.example.davaleba20.adapters.viewPagerAdapter
import com.example.davaleba20.api.ResponseHandler
import com.example.davaleba20.databinding.ActivityMainBinding
import com.example.davaleba20.viewmodel.NewsViewModel

class MainActivity : AppCompatActivity() {

    private lateinit var binding : ActivityMainBinding

    private val adapter : viewPagerAdapter = viewPagerAdapter()

    private val viewModel :  NewsViewModel = NewsViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        init()
        setContentView(binding.root)
    }
    private fun init(){
        viewModel.init()
        observes()
        settingUp()
    }
    private fun settingUp(){
        binding.viewwwager.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        binding.viewwwager.adapter = adapter
    }
    private fun observes(){
        viewModel.newsDataLiveData.observe(this, {
            when (it) {
                is ResponseHandler.Success -> adapter.getItems(it.data!!)
                is ResponseHandler.Error -> Toast.makeText(
                    this,
                    "${it.errorMessage}",
                    Toast.LENGTH_SHORT
                ).show()
            }
        })

    }
}
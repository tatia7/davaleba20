package com.example.davaleba20.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.davaleba20.api.ResponseHandler
import com.example.davaleba20.api.RetrofitService
import com.example.davaleba20.model.NewsModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class NewsViewModel : ViewModel() {

    private var _newsLiveData = MutableLiveData<ResponseHandler<List<NewsModel>>>()

    val newsDataLiveData: LiveData<ResponseHandler<List<NewsModel>>> = _newsLiveData

    fun init() {
        viewModelScope.launch {
            withContext(Dispatchers.IO){
                getNewsResponse()
            }
        }

    }

    private suspend fun getNewsResponse() {
        val result = RetrofitService.getAllNews().getNews()

        if (result.isSuccessful) {
            _newsLiveData.postValue(ResponseHandler.Success(result.body()))
        } else {
            result.code()
            ResponseHandler.Loading(false)
            ResponseHandler.Error("${result.errorBody()}")
        }
    }
}
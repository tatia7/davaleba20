package com.example.davaleba20.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.davaleba20.databinding.DetailsViewBinding

class recViewAdapter (private val summery : String,private val provider : String) : RecyclerView.Adapter<recViewAdapter.ViewHolder>(){

    inner class ViewHolder(val binding : DetailsViewBinding): RecyclerView.ViewHolder(binding.root){


        fun bind(){

            binding.desc.text = summery
            binding.timeline.text = provider
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DetailsViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }
    override fun getItemCount() = 1

}
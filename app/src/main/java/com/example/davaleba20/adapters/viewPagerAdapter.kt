package com.example.davaleba20.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.davaleba20.databinding.ItemViewBinding
import com.example.davaleba20.model.NewsModel
import com.example.davaleba20.viewmodel.NewsViewModel

class viewPagerAdapter : RecyclerView.Adapter<viewPagerAdapter.ViewHolder>() {


    private lateinit var recview: recViewAdapter
    private var newsList = mutableListOf<NewsModel>()

    inner class ViewHolder(private val binding: ItemViewBinding) : RecyclerView.ViewHolder(binding.root) {

        private lateinit var model: NewsModel
        private var provider: String = ""


        fun bind() {

            model = newsList[adapterPosition]

            binding.title.text = model.title.toString()
            binding.updateAt.text = model.updatedAt.toString()

            Glide.with(binding.picture.context).load(model.imageUrl)
                .into(binding.picture)

            for (i in 0..(model.events?.size!!)) {
                if (model.events?.size == 0) {
                    provider = "Null"
                } else {
                    provider = provider + "," + model.events?.get(i)?.provider.toString()
                }
            }
            recycler(model.summary.toString(), model.events.toString())
        }
        private fun recycler(summary: String, events: String) {
            recview = recViewAdapter(summary, events)
            binding.recView.layoutManager = LinearLayoutManager(binding.root.context)
            binding.recView.adapter = recview
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ItemViewBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind()
    }
    fun getItems(newslist: List<NewsModel>) {
        this.newsList.clear()
        this.newsList.addAll(newslist)
        notifyDataSetChanged()
    }
    override fun getItemCount() = newsList.size
}